# README #

### Currency converter - Basic info

Currency converter uses API of currencylayer.com to execute historic currency
conversions. User needs to register and login to system and then he/she will 
be able to execute conversions. Last five conversions will be shown underneath 
conversion form depending on user who is using the app.

###Usage

- open port 8080 on localhost
- available links: /register, /login, /converter, /h2
- app uses h2 in memory database
- password for h2 db is: admin

####Testing
Not all acceptance tests are implemented. It is ongoing work. Cucumber is used for testing
Running tests: mvn clean test
 
