package com.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CconverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CconverterApplication.class, args);

	}
}
