package com.challenge.controllers;

import com.challenge.domain.User;
import com.challenge.domain.forms.LoginForm;
import com.challenge.services.user.UserService;
import com.challenge.validator.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.challenge.helpers.ControllersHelper.authenticatedUser;

@Controller
public class AuthenticationController {

    private UserService userService;
    private LoginValidator loginValidator;

    @Autowired
    public void setLoginValidator(LoginValidator loginValidator) {
        this.loginValidator = loginValidator;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/login")
    public String loginUserPage(Authentication auth, Model model){

        if(auth != null){
            return "redirect:/converter";
        }

        model.addAttribute("email", "");
        model.addAttribute("password", "");

        return "authentication";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginUser(@ModelAttribute("loginForm") LoginForm loginForm, BindingResult bindingResult, Model model){
        loginValidator.validate(loginForm, bindingResult);

        if(bindingResult.hasErrors()){
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            for (FieldError error : allErrors){
                String field = error.getField();
                model.addAttribute(field + "Error", bindingResult.getFieldError(field).getDefaultMessage());
            }

            return "authentication";
        }

        User user = userService.getUserByEmailAddress(loginForm.getEmail());

        authenticatedUser(user);

        return "redirect:/converter";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }


}
