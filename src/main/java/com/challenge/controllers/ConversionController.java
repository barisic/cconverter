package com.challenge.controllers;

import com.challenge.domain.Conversion;
import com.challenge.domain.User;
import com.challenge.services.conversion.ConversionService;
import com.challenge.validator.ConversionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@PropertySource(value = "currency_layer_api.properties")
public class ConversionController {

    private ConversionService conversionService;
    private ConversionValidator conversionValidator;

    @Autowired
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Autowired
    public void setConversionValidator(ConversionValidator conversionValidator) {this.conversionValidator = conversionValidator;}

    @Value("${SupportedCurrencies}")
    String[] supportedCurrencies;

    @RequestMapping("/")
    public String homePage(Authentication auth, Model model){
        if(auth != null){
            User user = (User)auth.getPrincipal();
            String[] emailSplit = user.getEmail().split("@");
            String username = emailSplit[0];
            model.addAttribute("username", username);

            return "index-registered";
        }

        return "index";
    }

    @RequestMapping("/converter")
    public String converterPage(Authentication auth, Model model){
        if(auth == null){
            return "redirect:/login";
        }

        User user = (User)auth.getPrincipal();
        String[] emailSplit = user.getEmail().split("@");
        String username = emailSplit[0];

        model.addAttribute("conversion", new Conversion());
        model.addAttribute("currencies", supportedCurrencies);
        model.addAttribute("username", username);

        Iterable<Conversion> lastFiveConversions = conversionService.getLastFiveConversionsBy(user.getUserId());
        List<Conversion> lastFiveConversionsList = new ArrayList<>();

        if (lastFiveConversions != null){
            for (Conversion conversion : lastFiveConversions) {
                lastFiveConversionsList.add(conversion);
            }
            model.addAttribute("lastFiveConversions", lastFiveConversionsList);
        }

        return "converter";
    }

    @RequestMapping(value = "/conversion", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String convertCurrency(@ModelAttribute("conversion") Conversion conversionFormData, BindingResult bindingResult,  Model model, Authentication auth){

        User user = (User)auth.getPrincipal();

        conversionValidator.validate(conversionFormData, bindingResult);

        if(bindingResult.hasErrors()){
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            for (FieldError error : allErrors){
                String field = error.getField();
                model.addAttribute(field + "Error", bindingResult.getFieldError(field).getDefaultMessage());
            }

            Iterable<Conversion> lastFiveConversions = conversionService.getLastFiveConversionsBy(user.getUserId());
            List<Conversion> lastFiveConversionsList = new ArrayList<>();

            if (lastFiveConversions != null){
                for (Conversion conversion : lastFiveConversions) {
                    lastFiveConversionsList.add(conversion);
                }
                model.addAttribute("lastFiveConversions", lastFiveConversionsList);
            }
            model.addAttribute("currencies", supportedCurrencies);

            return "converter";
        }

        String conversionResult = conversionService.convert(conversionFormData.getSourceCurrency(), conversionFormData.getTargetCurrency(), Double.parseDouble(conversionFormData.getAmount()), conversionFormData.getConversionRateDate());

        conversionFormData.setUserId(user.getUserId());
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate todayDate = LocalDate.now();
        conversionFormData.setConversionTime(dateTimeFormatter.format(todayDate));

        conversionFormData.setResult(conversionResult);
        conversionService.save(conversionFormData);

        model.addAttribute("currencies", supportedCurrencies);

        return "redirect:/converter";
    }
}