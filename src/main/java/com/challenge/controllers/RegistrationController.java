package com.challenge.controllers;

import com.challenge.domain.User;
import com.challenge.services.user.UserService;
import com.challenge.validator.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.List;

import static com.challenge.helpers.ControllersHelper.generateListOfCountries;

@Controller
public class RegistrationController {

    private RegistrationValidator registrationValidator;

    private UserService userService;

    @Autowired
    public void setRegistrationValidator(RegistrationValidator registrationValidator) {
        this.registrationValidator = registrationValidator;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/register")
    public String registerUserPage(Model model){
        model.addAttribute("user", new User());
        model.addAttribute("countries", generateListOfCountries());

        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") User user, BindingResult bindingResult, Model model){

        registrationValidator.validate(user, bindingResult);

        if(bindingResult.hasErrors()){
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            for (FieldError error : allErrors){
                String field = error.getField();
                model.addAttribute(field + "Error", bindingResult.getFieldError(field).getDefaultMessage());
            }
            model.addAttribute("countries", generateListOfCountries());

            return "registration";
        }
        userService.register(user);

        return "redirect:/login";
    }
}
