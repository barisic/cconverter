package com.challenge.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "conversions")
public class Conversion {
    private static List<String> listCurrencies = null;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String userId;
    private String sourceCurrency;
    private String targetCurrency;
    private String amount;
    private String conversionRateDate;
    private String conversionTime;
    private String result;
    @Transient
    private String[] currencies;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getUserId(){
        return userId;
    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    public String getSourceCurrency(){
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency){
        this.sourceCurrency = sourceCurrency;
    }

    public String getTargetCurrency(){
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency){
        this.targetCurrency = targetCurrency;
    }

    public String getAmount(){
        return amount;
    }

    public void setAmount(String amount){
        this.amount = amount;
    }

    public String getResult(){
        return result;
    }

    public void setResult(String result){
        this.result = result;
    }

    public String getConversionRateDate(){
        return conversionRateDate;
    }

    public void setConversionRateDate(String conversionRateDate){
        this.conversionRateDate = conversionRateDate;
    }

    public String getConversionTime(){
        return conversionTime;
    }

    public void setConversionTime(String conversionTime){
        this.conversionTime = conversionRateDate;
    }

    public void setSupportedCurrencies(String[] currencies) {
        this.currencies = currencies;
    }

    public String[] getSupportedCurrencies() {

//        if(listCurrencies == null){
//            listCurrencies = new LinkedList<>(Arrays.asList(currencies.replaceAll(" ", "").split(",")));
//            listCurrencies = listCurrencies.stream().distinct().collect(Collectors.toList());
//            listCurrencies.sort(String::compareTo);
//        }

        return currencies;
    }
}
