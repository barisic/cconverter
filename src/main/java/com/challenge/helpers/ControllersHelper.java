package com.challenge.helpers;

import com.challenge.domain.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;

public class ControllersHelper {

    private static List<String> countries;

    public static List<String> generateListOfCountries(){
        if(countries == null){
            String[] locales = Locale.getISOCountries();
            countries = new ArrayList<>();
            for (String countryCode : locales) {
                Locale obj = new Locale("", countryCode);
                countries.add(obj.getDisplayCountry());
            }
            countries.sort(String::compareTo);
        }

        return countries;
    }

    public static void authenticatedUser(User user) {
        if(SecurityContextHolder.getContext().getAuthentication() != null)
            SecurityContextHolder.clearContext();

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
