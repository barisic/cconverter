package com.challenge.repositories;

import com.challenge.domain.Conversion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface ConversionDao extends JpaRepository<Conversion, Integer> {
    @Query(value = "SELECT * FROM ( SELECT TOP (5) * FROM CONVERSIONS c WHERE c.user_id = ?1 ORDER BY c.id DESC\n) ORDER BY id ASC", nativeQuery = true)
    List<Conversion> getLastFiveConversionsBy(String id);
    @Query(value = "SELECT * FROM CONVERSIONS c WHERE c.user_id = ?1 ORDER BY c.conversion_time ASC", nativeQuery = true)
    List<Conversion> getAllBy(String id);
}
