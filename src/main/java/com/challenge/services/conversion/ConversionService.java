package com.challenge.services.conversion;

import com.challenge.domain.Conversion;

public interface ConversionService {
    String convert(String source, String target, Double amount, String conversionRateDate);
    Conversion save(Conversion conversion);
    Iterable<Conversion> getLastFiveConversionsBy(String userId);
}
