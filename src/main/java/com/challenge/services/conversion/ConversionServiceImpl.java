package com.challenge.services.conversion;

import com.challenge.domain.Conversion;
import com.challenge.repositories.ConversionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

@Service
@PropertySource(value = "currency_layer_api.properties")
public class ConversionServiceImpl implements ConversionService {
    @Value("${ConversionURI}")
    private String conversionUri;

    @Value("${AccessKey}")
    private String accessKey;

    private ConversionDao conversionDao;

    @Autowired
    public void setUserDao(ConversionDao conversionDao) {
        this.conversionDao = conversionDao;
    }

    public String convert(String source, String target, Double amount, String conversionRateDate){

        RestTemplate restTemplate = new RestTemplate();

        String url = composeRESTFulUrlCall(conversionUri, "?access_key=", accessKey, "&from=", source, "&to=", target, "&amount=", amount.toString(), "&date=", conversionRateDate);
        HashMap response = restTemplate.getForObject(url, HashMap.class);

        return response.get("result").toString();
    }

    public Conversion save(Conversion conversion) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        conversion.setConversionRateDate(dtf.format(now));

        return conversionDao.save(conversion);
    }

    public Iterable<Conversion> getLastFiveConversionsBy(String userId) {
            if (conversionDao.getLastFiveConversionsBy(userId).size() < 5){
                return conversionDao.getAllBy(userId);
            }

        return conversionDao.getLastFiveConversionsBy(userId);
    }

    private String composeRESTFulUrlCall(String url, String...params){

        StringBuilder finalUrl = new StringBuilder();
        finalUrl.append(url);
        for(String param : params) finalUrl.append(param);
        return finalUrl.toString();
    }
}
