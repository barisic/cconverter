package com.challenge.services.user;

import com.challenge.domain.User;

public interface UserService {
    User register(User user);
    User getUserByEmailAddress(String email);
}

