package com.challenge.services.user;

import com.challenge.domain.User;
import com.challenge.repositories.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public User register(User user) {
        String userId = UUID.randomUUID().toString();
        user.setUserId(userId);
        return userDao.save(user);
    }

    public User getUserByEmailAddress(String email) {
        return userDao.findByEmailAddress(email);
    }
}
