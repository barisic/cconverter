package com.challenge.validator;

import com.challenge.domain.Conversion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Component
@PropertySource(value = "validation.properties")
public class ConversionValidator implements Validator {

    @Value("${NotEmpty}")
    String fieldEmptyMessage;

    @Value("${ConversionForm.Select.currency}")
    String currencyNotSelectedMessage;

    @Value("${ConversionForm.ShouldBeNumber.amount}")
    String valueShouldBeNumberMessage;

    @Value("${DateNotInFuture}")
    String dateInFutureMessage;

    public boolean supports(Class<?> aClass) {
        return Conversion.class.equals(aClass);
    }

    public void validate(Object object, Errors errors){
        Conversion conversion = (Conversion) object;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "conversionRateDate", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sourceCurrency", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "targetCurrency", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amount", "NotEmpty", fieldEmptyMessage);

        if(!conversion.getConversionRateDate().isEmpty()){
            if(!LocalDate.now().isAfter(LocalDate.parse(conversion.getConversionRateDate()))){
                errors.rejectValue("conversionRateDate", "DateNotInFuture", dateInFutureMessage);
            }
        }

        if(conversion.getSourceCurrency().equals("Select currency"))
            errors.rejectValue("sourceCurrency", "ConversionForm.Select.currency", currencyNotSelectedMessage);

        if(conversion.getTargetCurrency().equals("Select currency"))
            errors.rejectValue("targetCurrency", "ConversionForm.Select.currency", currencyNotSelectedMessage);

        if(!conversion.getAmount().matches("-?\\d+(\\.\\d+)?")){
                errors.rejectValue("amount", "ConversionForm.ShouldBeNumber.amount", valueShouldBeNumberMessage);
        }
    }
}
