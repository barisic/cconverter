package com.challenge.validator;

import com.challenge.domain.forms.LoginForm;
import com.challenge.domain.User;
import com.challenge.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.challenge.validator.ValidatorHelper.checkIfValidEmail;

@Component
@PropertySource(value = "validation.properties")
public class LoginValidator implements Validator {

    @Value("${NotEmpty}")
    String fieldEmptyMessage;

    @Value("${RFCStandards.email}")
    String invalidEmailMessage;

    @Value("${LoginForm.wrongPasswordOrUserDoesNotExistMessage}")
    String wrongPasswordOrUserDoesNotExistMessage;

    @Value("${LoginForm.password}")
    String wrongPasswordMessage;

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
        public boolean supports(Class<?> aClass) {
            return LoginForm.class.equals(aClass);
        }

        @Override
        @SuppressWarnings("unchecked")
    public void validate(Object object, Errors errors) {

        LoginForm loginForm = (LoginForm) object;

        String email = loginForm.getEmail();
        String password = loginForm.getPassword();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty", fieldEmptyMessage);

        User user = userService.getUserByEmailAddress(email);

        if (user != null){
            if(!checkIfValidEmail(user.getEmail())){
            errors.rejectValue("email", "RFCStandards.email", invalidEmailMessage);
        }

            if(!password.equals(user.getPassword())){
                errors.rejectValue("password", "LoginForm.password", wrongPasswordMessage);
            }
        }else{
            errors.rejectValue("password", "LoginForm.wrongPasswordOrUserDoesNotExistMessage", wrongPasswordOrUserDoesNotExistMessage);
        }
    }
}
