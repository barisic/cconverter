package com.challenge.validator;

import com.challenge.domain.User;
import com.challenge.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import java.time.LocalDate;

import static com.challenge.validator.ValidatorHelper.checkIfValidEmail;

@Component
@PropertySource(value = "validation.properties")
public class RegistrationValidator implements Validator {

    @Value("${NotEmpty}")
    String fieldEmptyMessage;

    @Value("${RFCStandards.email}")
    String invalidEmailMessage;

    @Value("${RegistrationForm.DifferentPassword.passwordConfirm}")
    String invalidPasswordConfirmMessage;

    @Value("${RegistrationForm.Select.country}")
    String countryNotSelectedMessage;

    @Value("${RegistrationForm.UserAlreadyExists.email}")
    String userAlreadyExistsMessage;

    @Value("${DateNotInFuture}")
    String dateInFutureMessage;

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        User user = (User) object;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfBirth", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zip", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "NotEmpty", fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty",fieldEmptyMessage);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordConfirm", "NotEmpty",fieldEmptyMessage);


        if(!checkIfValidEmail(user.getEmail()))
            errors.rejectValue("email", "RFCStandards.email", invalidEmailMessage);

        if(userService.getUserByEmailAddress(user.getEmail()) != null){
            errors.rejectValue("email", "RegistrationForm.UserAlreadyExists.email", userAlreadyExistsMessage);
        }

        if(!user.getDateOfBirth().isEmpty()){
            if(!LocalDate.now().isAfter(LocalDate.parse(user.getDateOfBirth()))){
                errors.rejectValue("dateOfBirth", "DateNotInFuture", dateInFutureMessage);
            }
        }

        if(user.getPasswordConfirm() != null){
            if(!user.getPasswordConfirm().equals(user.getPassword()))
                errors.rejectValue("passwordConfirm", "RegistrationForm.DifferentPassword.passwordConfirm", invalidPasswordConfirmMessage);
        }

        if(user.getCountry().equals("Select country")){
            errors.rejectValue("country", "RegistrationForm.Select.country", countryNotSelectedMessage);
        }
    }
}

