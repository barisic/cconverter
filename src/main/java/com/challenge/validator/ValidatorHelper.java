package com.challenge.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

@Component
class ValidatorHelper {
    static boolean checkIfValidEmail(String email){

        boolean result = true;
        EmailValidator validator = EmailValidator.getInstance();
        try {
            if (!validator.isValid(email))
                result = false;
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }
}
