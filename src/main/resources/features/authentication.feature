Feature: Authentication

  Registered user needs to login
  in order to use CConverter

  Scenario: Opening login page
    If user tries to open login page
    status code 200 should be returned
    and authentication page should be shown

    Given I try to open authentication page: "/login"
    Then Response status code should be 200
    And Authentication page should be opened: "authentication"


  Scenario: Authentication with valid data
    If authentication is successful, authenticated
    user should be redirected to converter page

    Given I am registered user
    When I try to login with input data:
      | email         | password |
      | test@test.com | dummyPass    |
    And There are zero errors
    Then I should be redirected to converter page: "redirect:/converter"


  Scenario: Authentication with invalid data
    If authentication is not successful,
    authentication page with errors should be shown

    Given I am registered user
    When I try to login with input data:
      | email         | password |
      | nouser@test.com | nopassword    |
    And There are existing errors
    Then I should be returned to authentication page with errors: "authentication"
