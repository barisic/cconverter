Feature: Conversion

  In order to convert amount between currencies
  user needs to submit source currency, target currency,
  enter and date.

  Scenario: Opening conversion page
  If user tries to open conversion page
  he should be redirected to login page

    Given I am authenticated user
    When I try to open conversion page: "/converter"
    Then I should be returned to converter page: "converter"


#  Scenario: Registration with valid data
#  If registration is successful, newly registered
#  user should be redirected to login page
#
#    Given I try to register with input data:
#      | email | dateOfBirth | address 	| zip	| city 	| country | password | passwordConfirm |
#      | test@test.com | 1986-02-24  | Street 22	| 21000	| Split	| Croatia | dummy    | dummy    |
#    When There are no errors
#    Then I should be redirected to login page: "redirect:/login"
#
#
#  Scenario: Registration with invalid data
#  If input data is invalid then registration page
#  with errors should be shown
#
#    Given I try to register with input data:
#      | email		  | dateOfBirth | country        |
#      | invalidEmail  | 2017-05-24  | Select country |
#    When There are errors
#    Then I should be returned to registration page with errors: "registration"