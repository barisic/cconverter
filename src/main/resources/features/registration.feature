Feature: Registration

  In order to use CConverter user needs to
  register with some valid input parameters:
  (email, date of birth, address, zip, city, country and password)

  Scenario: Opening registration page
    If user tries to open registration page
    status code 200 should be returned

    Given I try to open registration page: "/register"
    Then Status code should be 200
    And Registration page should be opened: "registration"


  Scenario: Registration with valid data
    If registration is successful, newly registered
    user should be redirected to login page

    Given I try to register with input data:
     | email | dateOfBirth | address 	| zip	| city 	| country | password | passwordConfirm |
     | test@test.com | 1986-02-24  | Street 22	| 21000	| Split	| Croatia | dummy    | dummy    |
    When There are no errors
    Then I should be redirected to login page: "redirect:/login"


  Scenario: Registration with invalid data
    If input data is invalid then registration page
    with errors should be shown

    Given I try to register with input data:
      | email		  | dateOfBirth | country        |
      | invalidEmail  | 2017-05-24  | Select country |
    When There are errors
    Then I should be returned to registration page with errors: "registration"