package com.challenge.controllers;

import com.challenge.domain.User;
import com.challenge.domain.forms.LoginForm;
import com.challenge.services.user.UserService;
import com.challenge.validator.LoginValidator;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import org.mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BeanPropertyBindingResult;
import java.util.List;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class AuthenticationControllerTestSteps {

    private MockMvc mockMvc;
    private BeanPropertyBindingResult errors;
    private ResultActions resultActions;

    @Mock
    private UserService userService;

    @InjectMocks
    private AuthenticationController authenticationController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        authenticationController = new AuthenticationController();
        mockMvc = MockMvcBuilders.standaloneSetup(authenticationController).build();
    }

    @Given("^I try to open authentication page: \"([^\"]*)\"$")
    public void I_try_to_open_authentication_page(String authUrl) throws Throwable{
        resultActions = mockMvc.perform(get(authUrl));
    }

    @Then("^Response status code should be (\\d+)$")
    public void Response_status_code_should_be(int statusCode) throws Throwable {
        resultActions
                .andExpect(model().attribute("email", ""))
                .andExpect(model().attribute("password", ""))
                .andExpect(status().is(statusCode));
    }

    @And("^Authentication page should be opened: \"([^\"]*)\"$")
    public void Authentication_page_should_be_opened(String authPage) throws Throwable {
        resultActions.andExpect(view().name(authPage));
    }

    @Given("^I am registered user$")
    public void I_am_registered_user() throws Throwable{
        User registeredUser = new User();
        registeredUser.setEmail("test@test.com");
        registeredUser.setDateOfBirth("1987-09-01");
        registeredUser.setAddress("Street");
        registeredUser.setZip("21000");
        registeredUser.setCity("Split");
        registeredUser.setCountry("Croatia");
        registeredUser.setPassword("dummyPass");

        Mockito.when(userService.getUserByEmailAddress("test@test.com")).thenReturn(registeredUser);
    }

    @When("^I try to login with input data:$")
    public void I_try_to_login_with_input_data(List<LoginForm> loginFormList) throws Throwable{
        LoginForm form = loginFormList.get(0);
        LoginValidator loginValidator = new LoginValidator();
        authenticationController.setLoginValidator(loginValidator);
        authenticationController.setUserService(userService);
        loginValidator.setUserService(userService);
        errors = new BeanPropertyBindingResult(form, "loginForm");
        loginValidator.validate(form, errors);

        resultActions = mockMvc.perform(post("/login")
                .param("email", form.getEmail())
                .param("password", form.getPassword()));
    }

    @And("^There are zero errors$")
    public void There_are_zero_errors() throws Throwable{
        assertFalse(errors.hasErrors());
    }

    @Then("^I should be redirected to converter page: \"([^\"]*)\"$")
    public void I_should_be_redirected_to_converter_page(String converterUrl) throws Throwable{
        resultActions
                .andExpect(status().isFound())
                .andExpect(view().name(converterUrl));
    }

    @And("^There are existing errors$")
    public void There_are_existing_errors() throws Throwable{
        assertTrue(errors.hasErrors());
    }

    @Then("^I should be returned to authentication page with errors: \"([^\"]*)\"$")
    public void I_should_be_returned_to_authentication_page(String authPage) throws Throwable{
        resultActions
                .andExpect(status().isOk())
                .andExpect(view().name(authPage))
                .andExpect(model().attributeHasFieldErrors("loginForm", "password"));
    }
}
