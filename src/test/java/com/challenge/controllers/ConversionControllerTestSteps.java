package com.challenge.controllers;

import com.challenge.domain.User;
import com.challenge.helpers.ControllersHelper;
import com.challenge.services.conversion.ConversionService;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import static org.junit.Assert.assertTrue;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
//
//public class ConversionControllerTestSteps {
//    private MockMvc mockMvc;
//    private ResultActions resultActions;
//    private User authenticatedUser;
//
//    @Mock
//    private ConversionService conversionService;
//
//    @InjectMocks
//    private ConversionController conversionController;
//
//    @Before
//    public void setup(){
//        MockitoAnnotations.initMocks(this);
//        conversionController = new ConversionController();
//        mockMvc = MockMvcBuilders.standaloneSetup(conversionController).build();
//
//        authenticatedUser = new User();
//        authenticatedUser.setEmail("test@test.com");
//        authenticatedUser.setDateOfBirth("1987/09/01");
//        authenticatedUser.setAddress("1267 Dora St");
//        authenticatedUser.setZip("80000");
//        authenticatedUser.setCity("France");
//        authenticatedUser.setCountry("France");
//        authenticatedUser.setPassword("dummy");
//
//    }
//
//    @Given("^I am authenticated user$")
//    public void I_am_authenticated_user() throws Throwable{
//        conversionController.setConversionService(conversionService);
//        Authentication auth;
//        ControllersHelper.authenticatedUser(authenticatedUser);
//        auth = SecurityContextHolder.getContext().getAuthentication();
//        assertTrue(auth != null);
//    }
//
//    @When("^I try to open conversion page: \"([^\"]*)\"$")
//    public void I_try_to_open_conversion_page(String authUrl) throws Throwable{
//        resultActions = mockMvc.perform(get(authUrl));
//    }
//
//    @Then("^I should be returned to converter page: \"([^\"]*)\"$")
//    public void I_should_be_returned_to_converter_page(String converterPage) throws Throwable {
//        resultActions
//                .andExpect(status().isOk())
//                .andExpect(view().name(converterPage));
//    }
//}
