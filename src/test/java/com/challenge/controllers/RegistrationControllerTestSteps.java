package com.challenge.controllers;

import com.challenge.domain.User;
import com.challenge.services.user.UserService;
import com.challenge.validator.RegistrationValidator;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.mockito.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BeanPropertyBindingResult;
import java.util.List;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

public class RegistrationControllerTestSteps {

    private MockMvc mockMvc;
    private BeanPropertyBindingResult errors;
    private ResultActions resultActions;

    @Mock
    private UserService userService;

    @InjectMocks
    private RegistrationController registrationController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        registrationController = new RegistrationController();
        mockMvc = MockMvcBuilders.standaloneSetup(registrationController).build();
    }

    @Given("^I try to open registration page: \"([^\"]*)\"$")
    public void I_try_to_open_registration_page(String registerUrl) throws Throwable {
        resultActions = mockMvc.perform(get(registerUrl));
    }

    @Then("^Status code should be (\\d+)$")
    public void Status_code_should_be(int statusCode) throws Throwable {
        resultActions.andExpect(status().is(statusCode));
    }

    @And("^Registration page should be opened: \"([^\"]*)\"$")
    public void Registration_page_should_be_opened(String registrationPage) throws Throwable {
        resultActions.andExpect(view().name(registrationPage));
    }

    @Given("^I try to register with input data:$")
    public void I_try_to_register_with_input_data(List<User> formUserList) throws Throwable {
        User user = formUserList.get(0);
        RegistrationValidator registrationValidator = new RegistrationValidator();
        registrationValidator.setUserService(userService);
        registrationController.setRegistrationValidator(registrationValidator);
        registrationController.setUserService(userService);
        errors = new BeanPropertyBindingResult(user, "user");
        registrationValidator.validate(user, errors);

        resultActions = mockMvc.perform(post("/register")
                .param("email", user.getEmail())
                .param("dateOfBirth", user.getDateOfBirth())
                .param("address", user.getAddress())
                .param("zip", user.getZip())
                .param("city", user.getCity())
                .param("country", user.getCountry())
                .param("password", user.getPassword())
                .param("passwordConfirm", user.getPasswordConfirm()));
    }

    @When("^There are no errors$")
    public void There_are_no_errors() throws Throwable {
        assertFalse(errors.hasErrors());
    }

    @Then("^I should be redirected to login page: \"([^\"]*)\"$")
    public void I_should_be_redirected_to_login_page(String loginUrl) throws Throwable {
        resultActions
                .andExpect(status().isFound())
                .andExpect(view().name(loginUrl));
    }

    @When("^There are errors$")
    public void There_are_errors() throws Throwable {
        assertTrue(errors.hasErrors());
    }

    @Then("^I should be returned to registration page with errors: \"([^\"]*)\"$")
    public void I_should_be_returned_to_registration_page_with_errors(String registerUrl) throws Throwable {
        resultActions
                .andExpect(status().isOk())
                .andExpect(view().name(registerUrl));
//                .andExpect(model().attributeHasFieldErrors("user","email", "dateOfBirth", "address", "zip", "city", "country", "password", "passwordConfirm"));
    }
}
